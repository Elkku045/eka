import sqlite3
from sqlite3 import Error
#https://www.sqlitetutorial.net/sqlite-python/insert/

#Tietokanta
def luo_yhteys(microbit_db):
    """ create a database connection to the SQLite database
        specified by db_file = microbit.db
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(microbit_db)
    except Error as e:
        print(e)

    return conn
    
def insert(conn, project):
    """
    Create a new project into the projects table
    :param conn:
    :param project:
    :return: project id
    """
    sql = ''' INSERT INTO taulu(aika,aani)
              VALUES(234,555) '''
    cur = conn.cursor()
    cur.execute(sql, project)
    conn.commit()
    return cur.lastrowid
    
aani = 0
"""diceNum = 0
sound = 0
enemyY = 6
enemyX = 6
enemySpeed = 400
playerY = 7
playerYplus = 7

def on_forever():
    global sound, playerY, playerYplus
    sound = input.sound_level()
    if sound < 40:
        led.plot(0, playerY)
        led.plot(0, playerYplus)
        playerY = 4
        playerYplus = 3
        led.unplot(0, 2)
    elif sound > 60:
        led.plot(0, playerY)
        led.plot(0, playerYplus)
        playerY = 3
        playerYplus = 2
        led.unplot(0, 4)
    else:
        led.plot(0, playerY)
        playerY = 4
        playerYplus = 7
        led.unplot(0, 3)
        led.unplot(0, 2)
    serial.write_value("sound", sound)
    basic.pause(500)
    if enemyX == 0:
        if playerY == enemyY:
            control.reset()
        if playerYplus == enemyY:
            control.reset()
basic.forever(on_forever)

def on_forever2():
    global enemyX, diceNum, enemyY, enemySpeed
    for index in range(15):
        basic.pause(enemySpeed)
        led.toggle(enemyX, enemyY)
        enemyX += -1
        led.toggle(enemyX, enemyY)
        if enemyX < 0:
            diceNum = randint(1, 4)
            if diceNum == 4:
                enemyY = 3
            else:
                enemyY = 4
            enemyX = 5
    if enemySpeed > 100:
        enemySpeed += -20
basic.forever(on_forever2)
"""

def main():
    database = "microbit.db"

    # create a database connection
    conn = luo_yhteys(database)
    with conn:
        project = ('time', aani);
        project_id = insert(conn, project)

if __name__ == '__main__':
    main()
